package TercerP.HackerMan2;

import java.util.concurrent.Callable;

public class ServerTask {
    public Server server;

    public ServerTask(Server server){
        this.server = server;
    }

    Callable<String[]> startForcing = () ->{
        try{
            String[] results = server.startForcing();
            return results;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    };
}
