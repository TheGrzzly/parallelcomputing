package TercerP.HackerMan2;

import SegundoP.Hackerman.SequentialPasswordForcer;

import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ParallelPasswordForcer {
    private hackermanui ui;
    private SequentialPasswordForcer sequential;
    private List<Server> servers;

    public static void main(String[] args) throws NoSuchAlgorithmException {
        ParallelPasswordForcer ppf = new ParallelPasswordForcer();
        ppf.sequential = new SequentialPasswordForcer();
        ppf.ui = new hackermanui(ppf.sequential, ppf);
    }

    private String getIP(){
        String ip = "";
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ip;
    }

    public void registerServer(String serverPath) {
        try {
            Server server = (Server) Naming.lookup("//" + serverPath);
            if (server.ping() != "hackerman")
                return;

            this.servers.add(server);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startForcing(String[] hashes) throws NoSuchAlgorithmException, RemoteException, InterruptedException, ExecutionException {
        long initTime = System.currentTimeMillis();
        List<ConcurrentPasswordForcer> forcers = new ArrayList<>();
        List<Callable<String[]>> taskList = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(servers.size());


        int howManyForEach = hashes.length / servers.size();

        int residual = hashes.length % servers.size();
        int iter = 0;
        for (Server server: servers){
            String[] hashForEach;
            if (residual <= 0)
                hashForEach = new String[howManyForEach];
            else
                hashForEach = new String[howManyForEach + 1];
            for (int i = 0; i <howManyForEach; i++){
                hashForEach[i] = hashes[iter];
                iter++;
            }
            if (residual > 0){
                hashForEach[hashForEach.length - 1] = hashes[iter];
                iter++;
                residual--;
            }
            ServerTask task = new ServerTask(server);
            taskList.add(task.startForcing);
        }

        List<Future<String[]>> futures = executorService.invokeAll(taskList);

        for (Future<String[]> future: futures){
            ui.updatePasswords(future.get());
        }

        executorService.shutdown();

        long endTime = System.currentTimeMillis();
        long result = endTime - initTime;
        System.out.println("Concurrent: The end time is " + result);
    }

}
