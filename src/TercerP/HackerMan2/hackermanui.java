package TercerP.HackerMan2;

import SegundoP.Hackerman.SequentialPasswordForcer;

import javax.swing.*;
import java.awt.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class hackermanui extends JFrame {

    private ConcurrentPasswordForcer concurrentPasswordForcer;
    private SequentialPasswordForcer sequentialPasswordForcer;
    private ParallelPasswordForcer parallelPasswordForcer;

    private String results = "";

    private JTextArea resultArea;

    public hackermanui(SequentialPasswordForcer sequentialForcer, ParallelPasswordForcer parallelForcer){
        super("hackerman");

        this.setSize(800, 500);
        this.setResizable(false);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.sequentialPasswordForcer = sequentialForcer;
        this.parallelPasswordForcer = parallelForcer;

        Container startingPanel = this.getContentPane();

        JLabel hashesLabel = new JLabel("Hashes");
        hashesLabel.setBounds(25, 25, 150, 30);
        startingPanel.add(hashesLabel);

        JLabel resultsLabel = new JLabel("Passwords");
        resultsLabel.setBounds(400, 25, 150, 30);
        startingPanel.add(resultsLabel);

        JTextArea hashesArea = new JTextArea(8, 22);
        JScrollPane hashesScrollPane = new JScrollPane(hashesArea);
        hashesScrollPane.setBounds(25, 50, 300, 250);
        startingPanel.add(hashesScrollPane);

        this.resultArea = new JTextArea(8, 22);
        JScrollPane resultsScrollPane = new JScrollPane(this.resultArea);
        resultsScrollPane.setBounds(400, 50, 300, 250);
        startingPanel.add(resultsScrollPane);

        JButton sequentialButton = new JButton("Sequential concurrent");
        sequentialButton.setBounds(25, 325, 200, 30);
        sequentialButton.addActionListener(event -> {
            try {
                this.forceSequentialPasswords(hashesArea.getText());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        });
        startingPanel.add(sequentialButton);

        JButton concurrentButton = new JButton("Resolve concurrent");
        concurrentButton.setBounds(25, 350, 200, 30);
        concurrentButton.addActionListener(event -> {
            try {
                this.forceConcurrentPasswords(hashesArea.getText());
            } catch (NoSuchAlgorithmException | InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
        startingPanel.add(concurrentButton);

        JButton parallelButton = new JButton("Resolve parallel");
        parallelButton.setBounds(25, 375, 200, 30);
        parallelButton.addActionListener(event ->{
            this.forceParallelPasswords(hashesArea.getText());
        });
        startingPanel.add(parallelButton);

        JTextField serverTextField = new JTextField(100);
        serverTextField.setBounds(400, 350, 200, 30);
        startingPanel.add(serverTextField);

        JButton serverAddButton = new JButton("Add Server");
        serverAddButton.setBounds(400, 375, 100, 30);
        serverAddButton.addActionListener(event ->{
            this.addServer(serverTextField.getText());
            serverTextField.setText("");
        });

        this.setVisible(true);
    }

    private void forceSequentialPasswords(String hashStrings) throws NoSuchAlgorithmException {
        String[] hashes = hashStrings.split("\n");
        System.out.println(hashes.length);
        for (String hash : hashes)
            System.out.println(hash);

        long initTime = System.currentTimeMillis();

        for (String hash : hashes) {
            sequentialPasswordForcer = new SequentialPasswordForcer();
            sequentialPasswordForcer.setHash(hash);
            this.results += hash + " = " + sequentialPasswordForcer.ForcePassword() + "\n";
            this.resultArea.setText(results);
        }

        long endTime = System.currentTimeMillis();
        long result = endTime - initTime;
        System.out.println("Sequential: The end time is " + result);
    }

    private void forceConcurrentPasswords(String hashStrings) throws NoSuchAlgorithmException, InterruptedException, ExecutionException {
        String[] hashes = hashStrings.split("\n");

        long initTime = System.currentTimeMillis();
        List<ConcurrentPasswordForcer> forcers = new ArrayList<>();
        List<Callable<String>> taskList = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (String hash : hashes){
            ConcurrentPasswordForcer forcer = new ConcurrentPasswordForcer(hash);
            forcers.add(forcer);
            taskList.add(forcers.get(forcers.size() - 1).run);
        }

        List<Future<String>> futures = executorService.invokeAll(taskList);

        int iterador = 0;
        for(Future<String> future: futures) {
            this.results += hashes[iterador] + " = " + future.get() + "\n";
            this.resultArea.setText(results);
            iterador++;
        }

        executorService.shutdown();

        long endTime = System.currentTimeMillis();
        long result = endTime - initTime;
        System.out.println("Concurrent: The end time is " + result);
    }

    private void forceParallelPasswords(String hashStrings){
        String[] hashes = hashStrings.split("\n");


    }

    private void addServer(String path){
        this.parallelPasswordForcer.registerServer(path);
    }

    public void updatePasswords(String[] passwords){
        for (String password: passwords){
            this.results += password +  "\n";
            this.resultArea.setText(results);
        }
    }
}
