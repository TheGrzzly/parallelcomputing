package TercerP.Server;

import TercerP.HackerMan2.ConcurrentPasswordForcer;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ParallelForcer extends UnicastRemoteObject implements Server {
    private String hashes[];
    private String endpoint = "/Forcer";

    public ParallelForcer(String ip, int port) throws RemoteException, NoSuchAlgorithmException {
        try {
            LocateRegistry.createRegistry(port);
            Naming.rebind("//" + ip + ":" + port + this.endpoint, this);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String ping() throws RemoteException {
        System.out.println("hackerman");

        return "hackerman";
    }

    @Override
    public void setHash(String[] hashes) throws RemoteException {
        this.hashes = hashes;
//        forcer.setHash(hash);
    }

    @Override
    public String[] startForcing() throws RemoteException, ExecutionException, InterruptedException, NoSuchAlgorithmException {
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        Future<String> future = executorService.submit(forcer.run);
//        String result = future.get();
//        return result;

        long initTime = System.currentTimeMillis();
        List<TercerP.HackerMan2.ConcurrentPasswordForcer> forcers = new ArrayList<>();
        List<Callable<String>> taskList = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (String hash : hashes){
            TercerP.HackerMan2.ConcurrentPasswordForcer forcer = new ConcurrentPasswordForcer(hash);
            forcers.add(forcer);
            taskList.add(forcers.get(forcers.size() - 1).run);
        }

        List<Future<String>> futures = executorService.invokeAll(taskList);

        String[] results = new String[hashes.length];

        int iterador = 0;
        for(Future<String> future: futures) {
            results[iterador] += hashes[iterador] + " = " + future.get();
            iterador++;
        }

        executorService.shutdown();

        long endTime = System.currentTimeMillis();
        long result = endTime - initTime;
        System.out.println("Concurrent: The end time is " + result);
        return results;
    }
}
