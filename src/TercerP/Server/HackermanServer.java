package TercerP.Server;

import java.net.InetAddress;

public class HackermanServer {
    private ServerUI ui;
    private ParallelForcer parallelForcer;

    public static void main(String[] args){
        HackermanServer hackerServer = new HackermanServer();
        hackerServer.ui = new ServerUI(hackerServer);
    }

    public String createParallelForcer(){
        try {
            this.parallelForcer = new ParallelForcer(this.getIP(), 6701);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return this.getIP() + ":6701/Forcer";
    }

    private String getIP(){
        String ip = "";
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ip;
    }


}
