package TercerP.Server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;

public interface Server extends Remote {
    public String ping() throws RemoteException;

    public void setHash(String[] hashes) throws RemoteException;
    public String[] startForcing() throws RemoteException, ExecutionException, InterruptedException, NoSuchAlgorithmException;
}
