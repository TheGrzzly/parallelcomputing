package TercerP.Server;

import javax.swing.*;
import java.awt.*;

public class ServerUI extends JFrame {
    private Container startingPanel;

    public ServerUI(HackermanServer hackerServer){
        super("Hackerman Server");

        this.setSize(400, 200);
        this.setResizable(false);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.startingPanel = this.getContentPane();

        JLabel ipLabel = new JLabel("IP: ");
        ipLabel.setBounds(25, 25, 350, 30);
        this.startingPanel.add(ipLabel);

        JButton startButton = new JButton("Start");
        startButton.setBounds(25, 100, 200, 30);
        startButton.addActionListener(event -> {
            String serverInfo = hackerServer.createParallelForcer();
            if (serverInfo.isEmpty()){
                System.out.println("Didn't get IP");
                return;
            }
            ipLabel.setText(ipLabel.getText() + serverInfo);
        });
        this.startingPanel.add(startButton);

        this.setVisible(true);
    }
}
