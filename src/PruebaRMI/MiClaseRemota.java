package PruebaRMI;

import java.rmi.RemoteException;

public class MiClaseRemota extends java.rmi.server.UnicastRemoteObject implements MiInterfazRemota {

    protected MiClaseRemota() throws RemoteException {
    }

    @Override
    public void miMetodo1() throws RemoteException {
        System.out.println("Estoy en el metodo 1");
    }

    @Override
    public int miMetodo2() throws RemoteException {
        System.out.println("Estoy en el metodo 1");
        return 5;
    }

    public static void main(String[] args){
        try{
            MiInterfazRemota mir = new MiClaseRemota();
            java.rmi.Naming.rebind("//" + java.net.InetAddress.getLocalHost().getHostAddress() + ":5890/PruebaRMI", mir);
        }catch (Exception e){

        }
    }

}
