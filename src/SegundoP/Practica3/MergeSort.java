package SegundoP.Practica3;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class MergeSort {
    public static void mergeSortSequential(int a[]){
        int[] tempArray = new int[a.length];
        mergeSortSequential( a, tempArray, 0, a.length-1);
    }

    private static void mergeSortSequential(int a[], int tempArray[], int left, int right){
        if(left < right){
            int center =  (left + right) / 2;
            mergeSortSequential(a, tempArray, left, center);
            mergeSortSequential(a, tempArray, center+1, right);
            mergeSequential(a, tempArray, left, center + 1, right);
        }
    }

    private static void mergeSequential(int a[], int tempArray[], int leftPos, int rightPos, int rightEnd){
        int leftEnd = rightPos -1;
        int tempPos = leftPos;
        int numElements = rightEnd - leftPos + 1;
        while(leftPos <= leftEnd && rightPos <=  rightEnd){
            if(a[leftPos] < (a[rightPos])){
                tempArray[tempPos++] = a[leftPos++];
            }else{
                tempArray[tempPos++] = a[rightPos++];
            }
        }
        while(leftPos <= leftEnd){
            tempArray[tempPos++] = a[leftPos++];
        }
        while(rightPos <= rightEnd){
            tempArray[tempPos++] = a[rightPos++];
        }
        for(int i = 0; i<numElements; i++, rightEnd--){
            a[rightEnd] = tempArray[rightEnd];
        }
    }

    public void sortConcurrent(int a[]){
        int[] helper = new int[a.length];
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        forkJoinPool.invoke(new MergeSortTask(a, helper, 0 , a.length-1));
    }

    private class MergeSortTask extends RecursiveAction{
        private static final long serialVersionUID = -749935388568367268L;
        private final int[] a;
        private final int[] helper;
        private final int lo;
        private final int hi;

        public MergeSortTask(int[] a, int[] helper, int lo, int hi){
            this.a = a;
            this.helper = helper;
            this.lo = lo;
            this.hi = hi;
        }

        @Override
        protected void compute(){
            if(lo<hi){
                int mid = (lo + hi) / 2;
                MergeSortTask left = new MergeSortTask(a, helper, lo, mid);
                MergeSortTask right = new MergeSortTask(a, helper, mid +1, hi);
                invokeAll(left, right);
                mergeConcurrent(this.a, this.helper, this.lo, mid + 1, this.hi);
            }else{
                return;
            }
        }

        private void mergeConcurrent(int a[], int[] helper, int lo, int mid, int hi){
            int leftEnd = mid - 1;
            int tempPos = lo;
            int num = hi - lo + 1;
            while(lo <= leftEnd && mid <= hi){
                if(a[lo] < (a[mid])){
                    helper[tempPos++] = a[lo++];
                }else{
                    helper[tempPos++] = a[mid++];
                }
            }
            while(lo <= leftEnd){
                helper[tempPos++] = a[lo++];
            }
            while(mid <= hi){
                helper[tempPos++] = a[mid++];
            }
            for(int i = 0; i < num; i++, hi--){
                a[hi] = helper[hi];
            }
        }
    }

    public static void main(String[] args){
        MergeSort m = new MergeSort();
        System.out.println("Let's see how much can we optimize this");
        Random rd = new Random();
        int[] arrInteger = new int[150_000_000];
        for(int i = 0; i<arrInteger.length; i++){
            arrInteger[i] = rd.nextInt();
        }
        long initTimeSequential = System.nanoTime();
        mergeSortSequential(arrInteger);
        long finalTimeSequential = System.nanoTime();
        long timeSequential =  finalTimeSequential - initTimeSequential;
        System.out.println("MergeSortSequential took " + timeSequential + " nanos");
        long initTimeConcurrent = System.nanoTime();
        m.sortConcurrent(arrInteger);
        long finalTimeConcurrent = System.nanoTime();
        long timeConcurrent = finalTimeConcurrent - initTimeConcurrent;
        System.out.println("MergeSortConcurrent took " + timeConcurrent + " nanos");
    }
}
