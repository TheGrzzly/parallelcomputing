package SegundoP.Practica1;

public class ThreadImplements implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++)
            System.out.println(i + " " +
                    Thread.currentThread().getName());

            System.out.println("Termina thread " +
                    Thread.currentThread().getName());
    }

    public static void main(String[] args){
        Thread t1 = new Thread(new ThreadImplements(), "Marco");
        Thread t2 = new Thread(new ThreadImplements(), "Gera");
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);
        t1.start();
        t2.start();
        System.out.println("Termina thread main");
    }
}
