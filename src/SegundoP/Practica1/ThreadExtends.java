package SegundoP.Practica1;

public class ThreadExtends extends Thread {
    public ThreadExtends(String str){
        super(str);
    }

    @Override
    public void run() {
        for(int i = 0; i < 10; i++){
            System.out.println(i + " " + getName());
        }
        System.out.println("Thread termina " + getName());
    }

    public static void main(String[] args){
        ThreadExtends t1 = new ThreadExtends("Marco");
        ThreadExtends t2 = new ThreadExtends("Gera");
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);
        System.out.println("Termina thread main");
    }
}
