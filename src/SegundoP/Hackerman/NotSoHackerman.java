package SegundoP.Hackerman;

import java.security.NoSuchAlgorithmException;

public class NotSoHackerman {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        String[] hashArray = new String[]{
//                "0415DA89C09252F789C297871931F5C6E0B04D94B70D0B70503B8734BB5D2749",
//                "AEFBFABA08E208442D22D33C416C62D972141DFDE5F644691C6222B58DE75773"
                 "A17BC13DCD7B76EA9677EBC5ACD5A1D4744713F25E51DD872BE9C29AE5D91D63"
        };

        String[] resultArray = new String[hashArray.length];

        long initTime = System.currentTimeMillis();

        SequentialPasswordForcer sequential = new SequentialPasswordForcer();
        String resultP = "";

        for (int i = 0; i<hashArray.length; i++){
            sequential.setHash(hashArray[i]);
            resultP = sequential.ForcePassword();
        }

        long endTime = System.currentTimeMillis();
        long result = endTime - initTime;
        System.out.println("The end time is " + result);

        for (int i = 0; i < hashArray.length; i++){
            System.out.println("El resultad del hash " + hashArray[i] + " es " + resultP);
        }

    }
}