package SegundoP.Hackerman;

import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Hackerman {
    public static void main(String[] args) throws NoSuchAlgorithmException, ExecutionException, InterruptedException {
        String[] hashArray = new String[]{
                //"A17BC13DCD7B76EA9677EBC5ACD5A1D4744713F25E51DD872BE9C29AE5D91D63"
                "4C4EA3B345F0C19B33C9D3AAF6D153E9C1AF26854F51A50C3F396199E4768BE8"
        };

        String[] resultArray = new String[hashArray.length];

        ExecutorService executorService = Executors.newFixedThreadPool(hashArray.length);
//
//        for (int i = 0; i<hashArray.length; i++){
//            executorService.submit(new ConcurrentPasswordForcer(hashArray[i], i, resultArray));
//        }
//
//        executorService.shutdown();

        ConcurrentPasswordForcer forcer = new ConcurrentPasswordForcer(hashArray[0]);
        Future<String> future = executorService.submit(forcer.run);


        String result = future.get();
        System.out.println("future gotten result: " + result);
    }
}
