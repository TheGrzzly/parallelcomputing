package SegundoP.Hackerman;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class SequentialPasswordForcer {
    private byte[] hashByte;
    private String hash;
    private String match = "";
    private boolean isDone = false;

    private static int length = 5;
    private static int minChar = 32;
    private static int maxChar = 126;

    private final MessageDigest digest = MessageDigest.getInstance("SHA-256");

    public SequentialPasswordForcer() throws NoSuchAlgorithmException {
    }

    public void setHash(String hash) throws NoSuchAlgorithmException{
        this.hash = hash;
        this.hashByte = DatatypeConverter.parseHexBinary(hash);
    }

    public String ForcePassword(){
        for(int letter = minChar; letter <= maxChar; letter++){
            System.out.println("trying to force with the char " + (char)letter);
            Force((char)letter);

            if (isDone){
                return this.match;
            }
        }
        return null;
    }

    private void Force(char initialCharacter){
        char[] chars = new char[this.length];
        chars[0] = initialCharacter;

        for (chars[1] = (char)minChar; chars[1] <= (char)maxChar && !this.isDone; chars[1]++){
            for (chars[2] = (char)minChar; chars[2] <= (char)maxChar && !this.isDone; chars[2]++){
                for (chars[3] = (char)minChar; chars[3] <= (char)maxChar && !this.isDone; chars[3]++){
                    for (chars[4] = (char)minChar; chars[4] <= (char)maxChar && !this.isDone; chars[4]++){
                        String attempt = new String(chars);

                        byte[] hash = digest.digest(attempt.getBytes());
                        if (Arrays.equals(hash, this.hashByte)){
                            this.match = attempt;
                            this.isDone = true;

                            break;
                        }
                    }
                }
            }
        }
        if(this.isDone)
            System.out.println("Found the password and it is :" + this.match);
    }
}
