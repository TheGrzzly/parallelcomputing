package SegundoP.Hackerman;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConcurrentPasswordForcer{
    protected final byte[] hashByte;
    private final String hash;
    protected String match = "";
    protected volatile boolean isDone = false;
    private final long iniTime;

    protected static int length = 5;
    protected static int minChar = 32;
    protected static int maxChar = 126;

    protected final MessageDigest digest = MessageDigest.getInstance("SHA-256");

    public ConcurrentPasswordForcer(String hash) throws NoSuchAlgorithmException {
        this.iniTime = System.currentTimeMillis();
        this.hash = hash;
        this.hashByte = DatatypeConverter.parseHexBinary(hash);
    }

    Callable<String> run = () -> {
        ExecutorService hashExecutor = Executors.newFixedThreadPool(16);
        for(int i = 32; i < 127; i++){
            try {
                System.out.println("Added a new task" + i+1);
                hashExecutor.submit(new Task(i, this));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        while(!isDone){
            Thread.sleep(1000);
        }
        hashExecutor.shutdown();
        return this.match;
    };

    public void ForcePassword(){
        for(int letter = minChar; letter <= maxChar; letter++){
            System.out.println("trying to force with the char " + (char)letter);
            Force((char)letter);

            if (isDone){
                break;
            }
        }
    }

    private void Force(char initialCharacter){
        char[] chars = new char[this.length];
        chars[0] = initialCharacter;
        for (chars[1] = (char)minChar; chars[1] <= (char)maxChar && !this.isDone; chars[1]++){
            for (chars[2] = (char)minChar; chars[2] <= (char)maxChar && !this.isDone; chars[2]++){
                for (chars[3] = (char)minChar; chars[3] <= (char)maxChar && !this.isDone; chars[3]++){
                    for (chars[4] = (char)minChar; chars[4] <= (char)maxChar && !this.isDone; chars[4]++){
                        String attempt = new String(chars);

                        byte[] hash = digest.digest(attempt.getBytes());
                        if (Arrays.equals(hash, this.hashByte)){
                            this.match = attempt;
                            this.isDone = true;

                            break;
                        }
                    }
                }
            }
        }
        if(this.isDone)
            System.out.println("Found the password and it is :" + this.match);
    }

    public boolean checkIsDone(){
        return  this.isDone;
    }

    public boolean checkPassword(String attempt){
        byte[] hash = digest.digest(attempt.getBytes());
        if (Arrays.equals(hash, this.hashByte)){
            this.match = attempt;
            this.isDone = true;

            long endTime = System.currentTimeMillis();

            long result = endTime - this.iniTime;
            System.out.println("The end time is " + result);

            return true;
        }
        return false;
    }
}

class Task implements Runnable {
    private int iterChar;
    private boolean iCompleted = false;
    private static ConcurrentPasswordForcer father;

    protected static int length = 5;
    protected static int minChar = 32;
    protected static int maxChar = 126;

    public Task(int taskNumber, ConcurrentPasswordForcer father) throws NoSuchAlgorithmException {
        this.iterChar = taskNumber;
        this.father = father;
    }

    @Override
    public void run(){
        char[] chars = new char[length];

        chars[0] = (char)this.iterChar;
        System.out.println("inital Iter char " + this.iterChar + " as " + (char)this.iterChar + " from " + Thread.currentThread().getName());
        for (chars[1] = (char)minChar; chars[1] <= (char)maxChar && !this.iCompleted; chars[1]++){
            System.out.println(chars[0] +  " " +chars[1] + " " + Thread.currentThread().getName());
            for (chars[2] = (char)minChar; chars[2] <= (char)maxChar && !this.iCompleted; chars[2]++){
                for (chars[3] = (char)minChar; chars[3] <= (char)maxChar && !this.iCompleted; chars[3]++){
                    for (chars[4] = (char)minChar; chars[4] <= (char)maxChar && !this.iCompleted; chars[4]++){
                        String attempt = new String(chars);

                        synchronized (father){
                            if(this.father.checkPassword(attempt)){
                                System.out.println("Found the password and it is :" + attempt);
                                this.iCompleted = true;
                                return;
                            }
                            if(this.father.checkIsDone()){
                                return;
                            }
                        }
                    }
                }
            }
            synchronized (father) {
                if (this.father.checkIsDone() && !this.iCompleted) {
                    return;
                }
            }
        }
    }
}