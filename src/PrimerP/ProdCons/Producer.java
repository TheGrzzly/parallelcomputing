package PrimerP.ProdCons;

import PrimerP.Process.Process;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Producer extends Process {
    static public List<Integer> list;
    static public Integer processNumber;
    public static ReentrantLock lock;
    public static Condition fullCondition;
    public static Condition emptyCondition;
    static public int Max;
    static public Integer prodTime;

    public Producer(List<Integer> list, Integer prodNum, ReentrantLock lock, Condition fullCondition, Condition emptyCondition, Integer limit, Integer prodTime){
        super();
        this.thread = new Thread(this::run);
        this.setStatus("new");
        this.list = list;
        this.processNumber = prodNum;
        this.lock = lock;
        this.type = "Producer";
        this.fullCondition = fullCondition;
        this.emptyCondition = emptyCondition;
        this.Max = limit;
        this.prodTime = prodTime;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if(this.thread.isInterrupted()){
                    this.JumpingF();
                    continue;
                }

                this.validatePause();
                lock.lock();
                if(list.size()>=this.Max) {
                    this.setStatus("waiting" + this.Max);
//                    System.out.println(this.getStatus());
                    fullCondition.await();
                }
                this.setStatus("Producing");
//                System.out.println(this.getStatus());
                addToList();
                Thread.sleep(prodTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                emptyCondition.signalAll();
                lock.unlock();
            }
        }
    }

    public void addToList(){
        Random rand = new Random();
        int x = rand.nextInt((10));
        list.add(x);
//        System.out.println("Producer " + processNumber + " Added " + x + " from the list");
    }
}
