package PrimerP.ProdCons;

import PrimerP.Process.Process;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Consumer extends Process{
    public static List<Integer> list;
    public static Integer processNumber;
    public static ReentrantLock lock;
    public static Condition fullCondition;
    public static Condition emptyCondition;
    public static Integer consTime;
    public static boolean Thinking;
    public static boolean Eating;


    public Consumer(List<Integer> list, Integer prodNum, ReentrantLock lock, Condition fullCondition, Condition emptyCondition, Integer consTime){
        super();
        this.thread = new Thread(this::run);
        this.setStatus("new");
        this.type = "Consumer";
        this.list = list;
        this.processNumber = prodNum;
        this.lock = lock;
        this.fullCondition = fullCondition;
        this.emptyCondition = emptyCondition;
        this.consTime = consTime;
    }

    @Override
    public void run(){
        while (true){
            try {
                if(this.thread.isInterrupted()){
                    this.JumpingF();
                    continue;
                }

                this.validatePause();
                lock.lock();
                if(list.isEmpty()){
                    this.setStatus("waiting");
//                    System.out.println(this.getStatus());
                    emptyCondition.await();
                }
                this.setStatus("Consuming");
//                System.out.println(this.getStatus());
                getItem();
                Thread.sleep(consTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                fullCondition.signalAll();
                lock.unlock();
            }
        }
    }

    public void getItem(){
        Random rand = new Random();
        int x = rand.nextInt(list.size());
        list.remove(x);
//        System.out.println("Consumer " + processNumber + " Took " + x + " from the list");
    }
}
