package PrimerP.ProdCons;

import PrimerP.Planner.Planner;
import PrimerP.ProdGraphics.Interactive;
import PrimerP.ProdGraphics.ProdConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ProdCons {
    public static void main(String[] args){
        Integer processCounter = 0;
        ReentrantLock lock = new ReentrantLock();
        Condition fullCondition = lock.newCondition();
        Condition emptyCondition = lock.newCondition();
        List<Integer> sharedList = new ArrayList<>();
        int limit = 10;
        Planner planner = new Planner(limit);
        Thread plannerThread = new Thread(planner);
        Integer prodTime = 400;
        Integer consTime = 400;

        for(int i = 0; i<5; i++){
            planner.addProcess(new Producer(sharedList, processCounter, lock, fullCondition, emptyCondition, limit, prodTime));
            processCounter++;
            planner.addProcess(new Consumer(sharedList, processCounter, lock, fullCondition, emptyCondition, consTime));
            processCounter++;
        }
        plannerThread.start();

        ProdConsumer UI = new ProdConsumer(planner, limit, sharedList,lock, fullCondition, emptyCondition, processCounter, prodTime, consTime);

        UI.run();
//        Interactive GUI = new Interactive();
//        GUI.setVisible(true);

//        ProdConsumer UI = new ProdConsumer();
    }
}
