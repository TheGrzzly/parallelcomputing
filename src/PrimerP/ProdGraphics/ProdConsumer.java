package PrimerP.ProdGraphics;

import PrimerP.Planner.Planner;
import PrimerP.ProdCons.Consumer;
import PrimerP.ProdCons.Producer;
import sun.jvm.hotspot.debugger.ProcessInfo;
import sun.tools.jps.Jps;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ProdConsumer extends JFrame implements Runnable {
    private JSpinner spinnerStorage;
    private JSpinner spinnerQuantum;
    private JButton btnAddProd;
    private JLabel lblNumProd;
    private JButton btnDelProd;
    private JLabel lblConsNum;
    private JButton btnAddCons;
    private JButton btnDelCons;
    private JTable tblProd;
    private JTable tblStorage;
    private JTable tblConsumer;
    private JPanel JPannelContainer;
    private JLabel storageLmt;
    private JLabel strCount;

    public Planner planner;
    public List<Integer> indexProd = new ArrayList<>();
    public List<Integer> indexCons = new ArrayList<>();
    public Integer procesCounter;

    public DefaultTableModel modelProd;
    public DefaultTableModel modelCons;
    public DefaultTableModel modelStorage;
    public static List<Integer> lists;
    public static int limit;

    public void setLimit(int i){
        this.limit = i;
    }


    public ProdConsumer(Planner planner, Integer Max, List<Integer> list, ReentrantLock lock, Condition eCondition, Condition fCondition, Integer processCounter, Integer prodTime, Integer consTime){
        this.add(JPannelContainer);

        this.planner = planner;
        for (int i = 0; i<10; i += 2){
            indexProd.add(i);
            indexCons.add(i+1);
        }
        procesCounter = processCounter;
        this.lists = list;
        this.limit = Max;
        btnAddProd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                planner.addProcess(new Producer(lists, procesCounter, lock, fCondition, eCondition, Max, prodTime));
                indexProd.add(procesCounter);
                procesCounter++;
            }
        });

        btnAddCons.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                planner.addProcess(new Consumer(lists, procesCounter, lock, fCondition, eCondition, consTime));
                indexCons.add(procesCounter);
                procesCounter++;
            }
        });

        btnDelProd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                planner.removeProcess("Producer");
                procesCounter--;
            }
        });

        btnDelCons.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                planner.removeProcess("Consumer");
                procesCounter--;
            }
        });

        setSpinners();

        spinnerStorage.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                planner.sharedInt = (int) spinnerStorage.getValue();
            }
        });

        spinnerQuantum.addChangeListener(e ->{
            planner.Quantum = (Integer) spinnerQuantum.getValue();
        });

        createTables();


        this.setTitle("Problema 1");
        this.setSize(500,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private void createTables(){
        String[] columnsProducer = {"Producers", "Status"};
        String[] columnsStorage = {"Storage"};
        String[] columnsConsumers = {"Consumers", "Status"};
        String[][] dataProd = {{"",""}};
        String[][] dataCons = {{"",""}};
        String[][] dataStorage = {{""}};

        modelProd = new DefaultTableModel(dataProd, columnsProducer);
        modelCons = new DefaultTableModel(dataCons, columnsConsumers);
        modelStorage = new DefaultTableModel(dataStorage, columnsStorage);

        tblProd.setModel(modelProd);

        tblConsumer.setModel(modelCons);

        tblStorage.setModel(modelStorage);

    }

    private void setSpinners(){
        SpinnerNumberModel modelQuantum = new SpinnerNumberModel(this.planner.Quantum, new Integer(100), new Integer(8000), new Integer(100));
        SpinnerNumberModel modelLimit = new SpinnerNumberModel(this.limit, 0, 50, 1);

        spinnerStorage.setModel(modelLimit);
        spinnerQuantum.setModel(modelQuantum);
    }

    @Override
    public void run() {
        while(true){
            try {
                ArrayList<String[]> processInfo = planner.getProcessInfo();
                int prodCount = 0;
                int consCount = 0;
                ArrayList<String[]> prodStatus = new ArrayList<>();
                ArrayList<String[]> consStatus = new ArrayList<>();

                for (String[] proces : processInfo){
                    if (proces[0].equals("Producer")){
                        prodCount++;
                        prodStatus.add(proces);
                    } else {
                        consCount++;
                        consStatus.add(proces);
                    }
                }

                lblNumProd.setText(""+prodCount);
                this.repaintComponent(lblNumProd);

                lblConsNum.setText(""+consCount);
                this.repaintComponent(lblConsNum);

                storageLmt.setText("Storage limit "+this.limit);
                this.repaintComponent(storageLmt);

                ArrayList<String[]> storage = new ArrayList<>();
                for(Integer stor : lists){
                    storage.add(new String[] {stor.toString()});
                }

                modelStorage = (DefaultTableModel) tblStorage.getModel();
                modelStorage.setRowCount(0);

                modelCons = (DefaultTableModel) tblConsumer.getModel();
                modelCons.setRowCount(0);

                modelProd = (DefaultTableModel) tblProd.getModel();
                modelProd.setRowCount(0);

                int storageCount = storage.size();
                strCount.setText("Storage: "+storageCount);
                repaintComponent(strCount);

                for (int i = 0; i<prodCount; i++)
                    modelProd.addRow(prodStatus.get(i));

                tblProd.setModel(modelProd);
                modelProd.fireTableDataChanged();

                this.repaintComponent(tblProd);

                for (int i = 0; i<consCount; i++)
                    modelCons.addRow(consStatus.get(i));

                tblConsumer.setModel(modelCons);
                modelCons.fireTableDataChanged();

                for (int i = 0; i<storageCount; i++)
                    modelStorage.addRow(storage.get(i));

                tblStorage.setModel(modelStorage);
                modelStorage.fireTableDataChanged();


                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("error UI");
            }
        }
    }

    private void repaintComponent(Component component){
        component.revalidate();
        component.repaint();
    }

//    public ProdConsumer(Planner planner, Integer limit, List<Integer> list, ReentrantLock lock, Condition eCondition, Condition fCondition, Integer processCounter, Integer prodTime, Integer consTime){
//        this.planner = planner;
//        for (int i = 0; i<10; i += 2){
//            indexProd.add(i);
//            indexCons.add(i+1);
//        }
//        procesCounter = processCounter;
//        btnAddProd.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                planner.addProcess(new Producer(list, procesCounter, lock, fCondition, eCondition, limit, prodTime));
//                indexProd.add(procesCounter);
//                procesCounter++;
//            }
//        });
//
//        btnAddCons.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                planner.addProcess(new Consumer(list, procesCounter, lock, fCondition, eCondition, consTime));
//                indexCons.add(procesCounter);
//                procesCounter++;
//            }
//        });
//
//        btnDelCons.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//            }
//        });
//    }
}