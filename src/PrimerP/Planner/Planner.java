package PrimerP.Planner;

import PrimerP.Process.Process;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Planner implements Runnable {
    public ArrayList<Process> list;
    private static ReentrantLock lock = new ReentrantLock();
    public int length = 0;
    public int counter = 0;
    public static Integer Quantum = 1000;
    private static Condition nowProcess = Planner.lock.newCondition();
    public static int sharedInt;

    public Planner(int sharedInt){
        list = new ArrayList<Process>();
        this.sharedInt = sharedInt;
    }

    public void addProcess(Process process){
//        try{
//            Planner.lock.lock();
            this.list.add(process);
            this.length = list.size();
//        }finally {
//            Planner.lock.unlock();
//        }
    }

    public void removeProcess(String type){
//        try{
//            Planner.lock.lock();
            for(int i = this.list.size() -1; i>=0; i--){
                if(this.list.get(i).type==type)
                    continue;

                if (this.list.get(i).getStatus() == "Consuming" || this.list.get(i).getStatus() == "Producing")
                    continue;

                this.list.get(i).thread.interrupt();
            }
//        }finally {
//            Planner.lock.unlock();
//        }
    }


    @Override
    public void run() {
        while(true){
            try {
                Process currentProcess = list.get(counter);

                if(currentProcess.thread.getState().toString().equals("NEW"))
                    currentProcess.thread.start();
                else
                    currentProcess.dePause();


                Thread.sleep(Quantum);
                currentProcess.pause();
                System.out.println(currentProcess.getStatus());
                counter++;
                if(counter>=list.size()) {
                    System.out.println("end of list");
                    counter=0;
                }
            } catch (InterruptedException e) {
                System.out.println("Error");
            }
        }
    }

    public String getStatusCurrentProcess(int index){
        Process currentProd = this.list.get(index);
        return currentProd.getStatus();
    }

    public String getStatusThread(int index){
        Process currentProd = this.list.get(index);
        return currentProd.thread.getState().toString();
    }

    public ArrayList<String[]> getProcessInfo(){
        ArrayList<String[]> info = new ArrayList<>();

//        try {
//            Planner.lock.unlock();
            for (Process proc : this.list)
                info.add(new String[] {proc.type, proc.getStatus()});
//        } finally {
//            Planner.lock.unlock();
//        }

        return info;
    }

    public ArrayList<String[]> getAditionalInfo(){
        ArrayList<String[]> info = new ArrayList<>();

        for (Process proc : this.list)
            info.add(proc.getInfo());

        return info;
    }
}
