package PrimerP.Dinner;

import PrimerP.DinnerUI.DinnerUI;
import PrimerP.Planner.Planner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class Dinner {

    public static void main(String []args){
        Planner taskManager = new Planner(0);
        List<ReentrantLock> forks = new ArrayList<ReentrantLock>();
        for (int i =0; i<5; i++)
            forks.add(new ReentrantLock());

        Thread taskManagerThread = new Thread(taskManager);
        taskManager.addProcess(new Philosopher(forks.get(0), forks.get(4), 1));
        taskManager.addProcess(new Philosopher(forks.get(1), forks.get(0), 2));
        taskManager.addProcess(new Philosopher(forks.get(2), forks.get(1), 3));
        taskManager.addProcess(new Philosopher(forks.get(3), forks.get(2), 4));
        taskManager.addProcess(new Philosopher(forks.get(4), forks.get(3), 5));

        taskManagerThread.start();

        DinnerUI UI = new DinnerUI(taskManager);

        UI.run();
    }
}
