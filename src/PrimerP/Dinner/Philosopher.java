package PrimerP.Dinner;

import PrimerP.Process.Process;

import javax.jws.Oneway;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Philosopher extends Process {
    static public ReentrantLock RightForkLock;
    static public ReentrantLock LeftForkLock;
    public int SleepAndThinkTime;
    public int ProcessNumber;
    static public ReentrantLock TimeLock;
    static public boolean Thonk;
    static public boolean Borgar;
    static public boolean rightHandBusy;
    static public boolean leftHandBusy;


    public Philosopher(ReentrantLock rightForkLock, ReentrantLock leftForkLock, int processNumber) {
        super();
        this.thread = new Thread(this::run);
        this.setStatus("New");
        this.RightForkLock = rightForkLock;
        this.LeftForkLock = leftForkLock;
        this.SleepAndThinkTime = 400;
        this.ProcessNumber = processNumber;

        this.TimeLock = new ReentrantLock();
    }

    @Override
    public void run(){
        while(true){
            try{
                this.validatePause();

                int time = this.SleepAndThinkTime;

                this.setStatus("Thinking");
                this.thread.sleep(time);
                this.validatePause();

                System.out.println(this.getStatus() + this.ProcessNumber);

                if(!this.LeftForkLock.tryLock()){
                    this.setStatus("Waiting");
                    System.out.println(this.getStatus() + this.ProcessNumber);
                    continue;
                }

                this.setStatus("LeftFork");
                this.validatePause();

                System.out.println(this.getStatus() + this.ProcessNumber);


                if(!this.RightForkLock.tryLock()){
                    this.LeftForkLock.unlock();
                    this.setStatus("Waiting");
                    System.out.println(this.getStatus() + this.ProcessNumber);
                    continue;
                }

                this.setStatus("BothForks");
                System.out.println(this.getStatus() + this.ProcessNumber);
                this.validatePause();

                this.setStatus("Eating");
                System.out.println(this.getStatus() + this.ProcessNumber);
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                this.RightForkLock.unlock();
                this.LeftForkLock.unlock();
            }
        }
    }

    @Override
    public String[] getInfo(){
        String[] info = new String[2];
        info[0] = String.valueOf(this.ProcessNumber);
        info[1] = this.getStatus();
        return info;
    }

    @Override
    public void setTime(int time){
        try{
            this.TimeLock.lock();
            this.SleepAndThinkTime = time;
        }finally {
            this.TimeLock.unlock();
        }
    }

    @Override
    public int getProdTime(){
        try{
            this.TimeLock.lock();
            return this.SleepAndThinkTime;
        }finally {
            this.TimeLock.unlock();
        }
    }
}
