package PrimerP.DinnerUI;

import PrimerP.Dinner.Philosopher;
import PrimerP.Planner.Planner;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.ArrayList;

public class DinnerUI extends JFrame implements Runnable {
    private JLabel Phil1;
    private JLabel Stat1;
    private JSlider Slid1;
    private JLabel Phil2;
    private JLabel Stat2;
    private JLabel Phil3;
    private JLabel Stat3;
    private JLabel Phil4;
    private JLabel Stat4;
    private JLabel Phil5;
    private JLabel Stat5;
    private JSpinner spinnerQuantum;
    private JSlider Slid2;
    private JSlider Slid3;
    private JSlider Slid4;
    private JSlider Slid5;
    private JPanel Saver;

    static public Planner manager;

    public DinnerUI(Planner manager) {
        this.add(Saver);
        this.manager = manager;


        this.Slid1 = new JSlider();
        this.Slid1.setMaximum(4000);
        this.Slid1.setMinimum(100);
        this.Slid1.setValue(manager.list.get(0).getProdTime());

        this.Slid1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                manager.list.get(0).setTime(Slid1.getValue());
            }
        });

        Slid2.setValue(manager.list.get(1).getProdTime());

        this.Slid2.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                manager.list.get(1).setTime(Slid2.getValue());
            }
        });

        Slid3.setValue(manager.list.get(2).getProdTime());

        this.Slid3.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                manager.list.get(2).setTime(Slid3.getValue());
            }
        });

        Slid4.setValue(manager.list.get(3).getProdTime());

        this.Slid4.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                manager.list.get(3).setTime(Slid4.getValue());
            }
        });

        Slid5.setValue(manager.list.get(4).getProdTime());

        this.Slid5.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                manager.list.get(4).setTime(Slid4.getValue());
            }
        });

        this.setTitle("Problema 2");
        this.setSize(500,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    @Override
    public void run() {
        while(true){
            try{
                ArrayList<String[]> info = manager.getAditionalInfo();

                Phil1.setText("Filosofo "+ info.get(0)[0]);
                repaintComponent(Phil1);
                Stat1.setText(info.get(0)[1]);
                Slid1.setValue(manager.list.get(0).getProdTime());

                Phil2.setText("Filosofo "+ info.get(1)[0]);
                repaintComponent(Phil1);
                Stat2.setText(info.get(1)[1]);
                Slid2.setValue(manager.list.get(1).getProdTime());

                Phil3.setText("Filosofo "+ info.get(2)[0]);
                repaintComponent(Phil1);
                Stat3.setText(info.get(2)[1]);
                Slid3.setValue(manager.list.get(2).getProdTime());

                Phil4.setText("Filosofo "+ info.get(3)[0]);
                repaintComponent(Phil1);
                Stat4.setText(info.get(3)[1]);
                Slid4.setValue(manager.list.get(3).getProdTime());

                Phil5.setText("Filosofo "+ info.get(4)[0]);
                repaintComponent(Phil1);
                Stat5.setText(info.get(4)[1]);
                Slid5.setValue(manager.list.get(4).getProdTime());

                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("error UI");
            }
        }
    }

    private void repaintComponent(Component component){
        component.revalidate();
        component.repaint();
    }
}
