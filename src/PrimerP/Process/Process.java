package PrimerP.Process;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Process implements Runnable {

    public Thread thread;
    private String status;
    private String id;
    public Boolean paused;
    public ReentrantLock pauseLock;
    public Condition pauseCondition;
    public String type;

    public Process(){
        this.paused = false;
        this.pauseLock = new ReentrantLock();
        this.pauseCondition = this.pauseLock.newCondition();
    }

    @Override
    public void run() { }

    public String getStatus(){ return status; }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public void setStatus(String status){ this.status = status; }

    public void setThread(Thread thread){ this.thread = thread; }

    public void pause() { this.paused = true; }

    public void dePause() {
        try{
            this.pauseLock.lock();
            this.paused = false;
            this.setStatus("Queued");
            System.out.println(this.getStatus());
            this.pauseCondition.signal();
        } finally {
            this.pauseLock.unlock();
        }
    }

    public void validatePause() {
        try {
            this.pauseLock.lock();
            if (this.paused){
                this.setStatus("Paused");
                System.out.println(this.getStatus());
                this.pauseCondition.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            this.pauseLock.unlock();
        }
    }

    public void JumpingF(){

    }

    public String[] getInfo(){
        return new String[2];
    }

    public void setTime(int time){

    }

    public int getProdTime(){
        return 0;
    }
}